// Conditional Statements -allows us to control the flow of out program

// [SECTION] if else, else if , else statements- 

/*
Syntax : 
if (condition){
	statement
};


*/
// IF statement

let numA = 5;

if(numA > 3){
	console.log('hello');
}


let city = "New york";
if (city === "New york") {
	console.log ("Welcome to New york City");

}

// else if clause

/*
- Execute a statements if previos condition are false and if the specified condition is true

- The "else if" clause is option and can be added to capture addtion conditions to change the flow of a program

-The "else if" statement was no longer run because if statement was able to run and evaluate of the whole statement stops there

*/

let numB = 1;
if (numA < 3){
	console.log('hello');
} else if (numB > 0){
	console.log('world');
}

// =======================

if (numA > 3){
	console.log('hello');
} else if (numB > 0){
	console.log('world');
}




// ===============

// else if in string


city = "tokyo";

if (city === "New York") {
	console.log("welcome to new york city")
}else if(city === "tokyo"){

	console.log("welcome to tokyo, Japan");
}



// else statement

/*
-Execute a statement if all other conditions are false
- the "else" statement is optional and can be added to capture any other result to change the flow of a program.

*/


let numC = -5;
let numD = 7;

if(numC > 0){
	console.log('Hello');
}else if (numD === 0){

	console.log('World');
} else {

	console.log('Again')
}


// if, else if, and else statements with function


function determineTyphoonIntensity(windSpeed){
	if (windSpeed < 30){
		return 'not a typhoon yet'
	} else if (windSpeed <= 61){
		return 'Trophical depression detected.'
	}
	else if (windSpeed >=62 && windSpeed <=88){
		// && means "AND" operator
		return 'Tropical storm detected'

	}else if (windSpeed >= 89 && windSpeed <= 117){
		// "||" meas OR operator
		return ' Severe tropical Storm detected.'
	}else {
		return ' Typhoon Detected'
	}
}

let message = determineTyphoonIntensity(87);
console.log(message);


// ==================

if (message =='Tropical storm detected'){
	console.warn(message);
}

// [SECTION] Conditional (Ternary) Operator
// Single statement execution

/*
	- The conditional (ternary) operator takes in three operands
	1. condition
	2.expression to execute if the condition is thruthy
	3. expression to execute if the condition is falsy

	-Ternary operator is for shorthand code. Commonly used for single statement execustion where the result consists of only one line of code


	Syntax :
	(expression) ? ifTrue : ifFalse;


	- can be used as an alternative to an "if else" statement
	
*/
let t = 'yes';
let f = 'No'

// Single statement execution
let ternaryResult = (1 < 18) ? t : f // true : false if walang value yung true or false
console.log("Result of Ternary Operator: " + ternaryResult);



// ===============

// Multiple Statement Execution

let name;

function isOfLegalAge() {
    name = 'John';
    return 'You are of the legal age limit';
}

function isUnderAge() {
    name = 'Jane';
    return 'You are under the age limit';
}

// The "parseInt" function converts the input recieve into a number data type.


let age = parseInt(prompt("What is your age?"));
console.log(age);
let legalAge = (age >= 18) ? isOfLegalAge() : isUnderAge();
console.log("Result of Ternary Operator in functions: " + legalAge + ', ' + name);



// [SECTION] switch statement
/*
	Syntax:
	switch (expression){
	case value:
		statement;
		break;
		default : statement;
	}




	- The "switch" statement evaliates an expression and matches the expression's value to the case clause. the switch will then execute statements associated with that case , as well as statements in cases that follow the matching case.
	- can be used an alternative to an if, else if and else statement where the data to be used in the condition is of an expected output

	-the ".toLowerCase()" function/method will change input received from the prompt into all lower case letters ensuring a match with the switch case condition if the users inputs capitalized or uppercase letters.
*/
let day = prompt ("what day of the week is it today?").toLowerCase();
console.log(day);

switch (day) {
    case 'monday': 
        console.log("The color of the day is red");
        break;
    case 'tuesday':
        console.log("The color of the day is orange");
        break;
    case 'wednesday':
        console.log("The color of the day is yellow");
        break;
    case 'thursday':
        console.log("The color of the day is green");
        break;
    case 'friday':
        console.log("The color of the day is blue");
        break;
    case 'saturday':
        console.log("The color of the day is indigo");
        break;
    case 'sunday':
        console.log("The color of the day is violet");
        break;
    default:
        console.log("Please input a valid day");
        break;
}